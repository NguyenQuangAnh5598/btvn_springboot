package com.example.springboot.service;

import com.example.springboot.model.Student;

import java.util.List;
import java.util.Optional;

public interface IStudentService {
     List<Student> findAll();

     Student save(Student student);

     void remove(Long id);

     Optional<Student> findById(Long id);

}
